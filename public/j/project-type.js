$(document).ready(function() {
    $('a.overlay-link').hover(
        function() {
            $(this).find('.overlay').get(0).style.display = 'flex';
        },
        function() {
            $(this).find('.overlay').get(0).style.display = 'none';
        });
});